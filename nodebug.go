// +build !debug

package debug

type logger struct{}

// NewLogger returns a new logger instance
func NewLogger(pkgPrefix string) Logger { return (*logger)(nil) }

func (*logger) SetFlags(flags int)                  { /* no-op */ }
func (*logger) Log(v ...interface{})                { /* no-op */ }
func (*logger) Logf(fmt string, v ...interface{})   { /* no-op */ }
func (*logger) Print(v ...interface{})              { /* no-op */ }
func (*logger) Printf(fmt string, v ...interface{}) { /* no-op */ }
