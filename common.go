package debug

const (
	// Log the function name and package
	LogFunc = 1 << iota
	// Log the file name
	LogFile
	// Log the line in the file
	LogLine
)

// LogDefaults are the default logging flags
const LogDefaults = LogFunc

type Logger interface {
	SetFlags(flags int)

	Log(v ...interface{})
	Logf(fmt string, v ...interface{})

	Print(v ...interface{})
	Printf(fmt string, v ...interface{})
}
