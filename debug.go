// +build debug

package debug

import (
	"fmt"
	"log"
	"runtime"
	"strings"
)

type logger struct {
	logFunc, logFile, logLine bool

	pkgPrefix string
}

func NewLogger(pkgPrefix string) Logger {
	d := &logger{pkgPrefix: pkgPrefix}
	d.SetFlags(LogDefaults)
	return d
}

func (d *logger) SetFlags(flags int) {
	d.logFunc = flags&LogFunc != 0
	d.logFile = flags&LogFile != 0
	d.logLine = flags&LogLine != 0
}

func (d *logger) prefix() string {
	pc, _, _, ok := runtime.Caller(2)
	if !ok {
		return "???"
	}

	fn := runtime.FuncForPC(pc)

	var r []string
	if d.logFile || d.logLine {
		file, line := fn.FileLine(pc)
		if d.logFile {
			i := strings.LastIndex(file, "/")
			if i >= 0 {
				file = file[i+1:]
			}
			r = append(r, file)
		}
		if d.logLine {
			r = append(r, fmt.Sprint(line))
		}
	}

	if d.logFunc {
		s := fn.Name()

		if strings.HasPrefix(s, d.pkgPrefix) {
			s = s[len(d.pkgPrefix):]
		}

		i := strings.LastIndex(s, "/")
		if i >= 0 {
			s = s[:i+1] + strings.Replace(s[i+1:], ".", "·", -1)
		}
		r = append(r, s)
	}

	if len(r) == 0 {
		return ""
	}

	return "{" + strings.Join(r, ":") + "}"
}

func (d *logger) Log(v ...interface{}) {
	p := d.prefix()
	if p != "" {
		v = append([]interface{}{p}, v...)
	}
	log.Println(v...)
}

func (d *logger) Logf(format string, v ...interface{}) {
	p := d.prefix()
	if p == "" {
		log.Printf(format, v...)
		return
	}

	v = append([]interface{}{p}, v...)
	log.Printf("%s "+format, v...)
}

func (d *logger) Print(v ...interface{}) {
	fmt.Println(v...)
}

func (d *logger) Printf(format string, v ...interface{}) {
	fmt.Println(fmt.Sprintf(format, v...))
}
