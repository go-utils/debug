# Go Debug

[![GoDoc](https://godoc.org/gitlab.com/go-utils/debug?status.svg)](http://godoc.org/gitlab.com/go-utils/debug)

This module provides simple debugging tools that compile to no-ops unless built
with the tag `debug`.